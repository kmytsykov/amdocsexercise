package com.mytsykov.amdocsexercise.repository;

import com.mytsykov.amdocsexercise.domain.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
}
