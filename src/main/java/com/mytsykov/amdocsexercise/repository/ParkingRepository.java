package com.mytsykov.amdocsexercise.repository;

import com.mytsykov.amdocsexercise.domain.Parking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParkingRepository extends CrudRepository<Parking, Long> {
    List<Parking> findAll();

    List<Parking> findAllByDieselAllowedTrueAndNumberOfFreePrivatePlacesGreaterThan(Integer count);

    List<Parking> findAllByDieselAllowedTrueAndNumberOfFreeCommercialPlacesGreaterThan(Integer count);

    List<Parking> findAllByNumberOfFreeElectricPrivatePlacesGreaterThan(Integer count);

    List<Parking> findAllByNumberOfFreeElectricCommercialPlacesGreaterThan(Integer count);

    List<Parking> findAllByNumberOfFreePrivatePlacesGreaterThan(Integer count);

    List<Parking> findAllByNumberOfFreeCommercialPlacesGreaterThan(Integer count);
}
