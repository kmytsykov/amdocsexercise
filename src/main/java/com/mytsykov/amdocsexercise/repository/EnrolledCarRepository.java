package com.mytsykov.amdocsexercise.repository;

import com.mytsykov.amdocsexercise.domain.EnrolledCar;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrolledCarRepository extends CrudRepository<EnrolledCar, Long> {
}
