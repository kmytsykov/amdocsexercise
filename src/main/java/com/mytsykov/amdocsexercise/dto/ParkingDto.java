package com.mytsykov.amdocsexercise.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParkingDto {
    private Long id;
    private Double lat;
    private Double lon;
    private String name;
    private Integer maxNumberOfPrivateCars;
    private Integer maxNumberOfCommercialCars;
    private Integer maxNumberOfElectricPrivateCars;
    private Integer maxNumberOfElectricCommercialCars;
    private Boolean dieselAllowed;
}
