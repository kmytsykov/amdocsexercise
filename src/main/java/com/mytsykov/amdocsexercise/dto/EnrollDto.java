package com.mytsykov.amdocsexercise.dto;

import lombok.Data;

@Data
public class EnrollDto {
    private Double lat;
    private Double lon;
    private Integer searchRadius;
}
