package com.mytsykov.amdocsexercise.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IdDto {
    private Long id;
}
