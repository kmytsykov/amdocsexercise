package com.mytsykov.amdocsexercise.dto;

import com.mytsykov.amdocsexercise.domain.enums.CarType;
import com.mytsykov.amdocsexercise.domain.enums.EngineType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Specification {
    private EngineType engineType;
    private CarType carType;
}
