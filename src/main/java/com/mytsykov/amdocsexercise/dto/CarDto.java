package com.mytsykov.amdocsexercise.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CarDto {
    private Long id;
    private String brand;
    private Specification specification;
}
