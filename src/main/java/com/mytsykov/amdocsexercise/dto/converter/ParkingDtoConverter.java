package com.mytsykov.amdocsexercise.dto.converter;

import com.google.common.base.Converter;
import com.mytsykov.amdocsexercise.domain.Parking;
import com.mytsykov.amdocsexercise.dto.ParkingDto;
import org.springframework.stereotype.Component;

@Component
public class ParkingDtoConverter extends Converter<Parking, ParkingDto> {

    @Override
    protected ParkingDto doForward(Parking parking) {
        return ParkingDto.builder()
                .id(parking.getId())
                .lat(parking.getLat())
                .lon(parking.getLon())
                .name(parking.getName())
                .maxNumberOfCommercialCars(parking.getMaxNumberOfCommercialCars())
                .maxNumberOfPrivateCars(parking.getMaxNumberOfPrivateCars())
                .maxNumberOfElectricCommercialCars(parking.getMaxNumberOfElectricCommercialCars())
                .maxNumberOfElectricPrivateCars(parking.getMaxNumberOfElectricPrivateCars())
                .dieselAllowed(parking.getDieselAllowed())
                .build();
    }

    @Override
    protected Parking doBackward(ParkingDto parkingDto) {
        return Parking.builder()
                .lat(parkingDto.getLat())
                .lon(parkingDto.getLon())
                .name(parkingDto.getName())
                .maxNumberOfCommercialCars(parkingDto.getMaxNumberOfCommercialCars())
                .maxNumberOfPrivateCars(parkingDto.getMaxNumberOfPrivateCars())
                .maxNumberOfElectricCommercialCars(parkingDto.getMaxNumberOfElectricCommercialCars())
                .maxNumberOfElectricPrivateCars(parkingDto.getMaxNumberOfElectricPrivateCars())
                .dieselAllowed(parkingDto.getDieselAllowed())
                .build();
    }
}
