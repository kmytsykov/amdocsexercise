package com.mytsykov.amdocsexercise.dto.converter;

import com.google.common.base.Converter;
import com.mytsykov.amdocsexercise.domain.Car;
import com.mytsykov.amdocsexercise.dto.CarDto;
import com.mytsykov.amdocsexercise.dto.Specification;
import org.springframework.stereotype.Component;

@Component
public class CarDtoConverter extends Converter<Car, CarDto> {
    @Override
    protected CarDto doForward(Car car) {
        return CarDto.builder()
                .id(car.getId())
                .brand(car.getBrand())
                .specification(new Specification(car.getEngineType(), car.getCarType()))
                .build();
    }

    @Override
    protected Car doBackward(CarDto carDto) {
        return Car.builder()
                .brand(carDto.getBrand())
                .engineType(carDto.getSpecification().getEngineType())
                .carType(carDto.getSpecification().getCarType())
                .build();
    }
}
