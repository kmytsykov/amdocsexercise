package com.mytsykov.amdocsexercise.common.constant;

public interface ErrorMessage {
    String CAR_WITH_ID_NOT_FOUND = "Car with id = %d not found";
    String PARKING_NOT_FOUND = "Parking not found";
}
