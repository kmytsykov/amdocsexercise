package com.mytsykov.amdocsexercise.common.util;

public class DistanceHelper {
    private static final Double sphereRadius = 6371d;
    private static final Double factor = 2 * sphereRadius;

    public static Double getDistance(Double lat1, Double lon1, Double lat2, Double lon2) {
        return factor * Math.asin(
                Math.sqrt(Math.pow(Math.sin((lat2 - lat1) / 2), 2)
                        + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin((lon2 - lon1) / 2), 2))
        );
    }
}
