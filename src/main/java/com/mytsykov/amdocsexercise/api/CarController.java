package com.mytsykov.amdocsexercise.api;

import com.mytsykov.amdocsexercise.dto.CarDto;
import com.mytsykov.amdocsexercise.dto.IdDto;
import com.mytsykov.amdocsexercise.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public IdDto createCar(@RequestBody CarDto car) {
        return new IdDto(carService.create(car).getId());
    }
}
