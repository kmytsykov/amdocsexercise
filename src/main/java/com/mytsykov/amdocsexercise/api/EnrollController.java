package com.mytsykov.amdocsexercise.api;

import com.mytsykov.amdocsexercise.dto.EnrollDto;
import com.mytsykov.amdocsexercise.dto.IdDto;
import com.mytsykov.amdocsexercise.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/enroll")
public class EnrollController {

    private final EnrollService enrollService;

    @Autowired
    public EnrollController(EnrollService enrollService) {
        this.enrollService = enrollService;
    }

    @PostMapping("/{carId}")
    public IdDto enroll(@PathVariable Long carId, @RequestBody EnrollDto enrollData) {
        return new IdDto(enrollService.enroll(carId, enrollData).getId());
    }
}
