package com.mytsykov.amdocsexercise.api;

import com.mytsykov.amdocsexercise.dto.IdDto;
import com.mytsykov.amdocsexercise.dto.ParkingDto;
import com.mytsykov.amdocsexercise.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parking")
public class ParkingController {

    private final ParkingService parkingService;

    @Autowired
    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }

    @PostMapping
    public IdDto createParking(@RequestBody ParkingDto parking) {
        return new IdDto(parkingService.create(parking).getId());
    }
}
