package com.mytsykov.amdocsexercise.service;

import com.mytsykov.amdocsexercise.dto.ParkingDto;

public interface ParkingService {
    ParkingDto create(ParkingDto parking);
}
