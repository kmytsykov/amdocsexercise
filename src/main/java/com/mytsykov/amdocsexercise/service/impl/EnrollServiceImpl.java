package com.mytsykov.amdocsexercise.service.impl;

import com.google.common.base.Functions;
import com.google.common.collect.Ordering;
import com.mytsykov.amdocsexercise.common.util.DistanceHelper;
import com.mytsykov.amdocsexercise.domain.Car;
import com.mytsykov.amdocsexercise.domain.EnrolledCar;
import com.mytsykov.amdocsexercise.domain.Parking;
import com.mytsykov.amdocsexercise.domain.enums.CarType;
import com.mytsykov.amdocsexercise.domain.enums.EngineType;
import com.mytsykov.amdocsexercise.dto.EnrollDto;
import com.mytsykov.amdocsexercise.dto.ParkingDto;
import com.mytsykov.amdocsexercise.dto.converter.ParkingDtoConverter;
import com.mytsykov.amdocsexercise.repository.CarRepository;
import com.mytsykov.amdocsexercise.repository.EnrolledCarRepository;
import com.mytsykov.amdocsexercise.repository.ParkingRepository;
import com.mytsykov.amdocsexercise.service.EnrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mytsykov.amdocsexercise.common.constant.ErrorMessage.CAR_WITH_ID_NOT_FOUND;
import static com.mytsykov.amdocsexercise.common.constant.ErrorMessage.PARKING_NOT_FOUND;

@Service
public class EnrollServiceImpl implements EnrollService {

    private final CarRepository carRepository;
    private final ParkingRepository parkingRepository;
    private final EnrolledCarRepository enrolledCarRepository;
    private final ParkingDtoConverter parkingDtoConverter;

    @Autowired
    public EnrollServiceImpl(CarRepository carRepository,
                             ParkingRepository parkingRepository,
                             EnrolledCarRepository enrolledCarRepository,
                             ParkingDtoConverter parkingDtoConverter) {
        this.carRepository = carRepository;
        this.parkingRepository = parkingRepository;
        this.enrolledCarRepository = enrolledCarRepository;
        this.parkingDtoConverter = parkingDtoConverter;
    }

    @Override
    @Transactional
    public ParkingDto enroll(Long carId, EnrollDto enrollData) {
        Car car = carRepository.findById(carId)
                .orElseThrow(() -> new IllegalArgumentException(String.format(CAR_WITH_ID_NOT_FOUND, carId)));

        Parking targetParking = findAllByCarSpecification(car.getEngineType(), car.getCarType())
                .stream()
                .collect(Collectors.toMap(parking -> parking, parking -> DistanceHelper.getDistance(
                        enrollData.getLat(),
                        enrollData.getLon(),
                        parking.getLat(),
                        parking.getLon())))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .filter(e -> e.getValue() <= enrollData.getSearchRadius())
                .findFirst().orElseThrow(() -> new IllegalArgumentException(PARKING_NOT_FOUND))
                .getKey();


        EnrolledCar enrolledCar = new EnrolledCar(car, targetParking);
        enrolledCarRepository.save(enrolledCar);

        updateParkingLotAvailability(targetParking, car.getEngineType(), car.getCarType());
        targetParking = parkingRepository.save(targetParking);

        return parkingDtoConverter.convert(targetParking);
    }

    private List<Parking> findAllByCarSpecification(EngineType engineType, CarType carType) {
        if (engineType == EngineType.DIESEL) {
            return carType == CarType.PRIVATE
                    ? parkingRepository.findAllByDieselAllowedTrueAndNumberOfFreePrivatePlacesGreaterThan(0)
                    : parkingRepository.findAllByDieselAllowedTrueAndNumberOfFreeCommercialPlacesGreaterThan(0);

        } else if (engineType == EngineType.ELECTRIC) {
            return carType == CarType.PRIVATE
                    ? parkingRepository.findAllByNumberOfFreeElectricPrivatePlacesGreaterThan(0)
                    : parkingRepository.findAllByNumberOfFreeElectricCommercialPlacesGreaterThan(0);
        }

        return carType == CarType.PRIVATE
                ? parkingRepository.findAllByNumberOfFreePrivatePlacesGreaterThan(0)
                : parkingRepository.findAllByNumberOfFreeCommercialPlacesGreaterThan(0);
    }

    private void updateParkingLotAvailability(Parking parking, EngineType engineType, CarType carType) {
        if (engineType == EngineType.DIESEL) {
            if (carType == CarType.PRIVATE) {
                parking.setNumberOfFreePrivatePlaces(parking.getMaxNumberOfPrivateCars() - 1);
            } else {
                parking.setNumberOfFreeCommercialPlaces(parking.getMaxNumberOfCommercialCars() - 1);
            }
        } else if (engineType == EngineType.ELECTRIC) {
            if (carType == CarType.PRIVATE) {
                parking.setNumberOfFreeElectricPrivatePlaces(parking.getNumberOfFreeElectricPrivatePlaces() - 1);
            } else {
                parking.setNumberOfFreeElectricCommercialPlaces(parking.getNumberOfFreeElectricCommercialPlaces() - 1);
            }
        }

        if (carType == CarType.PRIVATE) {
            parking.setNumberOfFreePrivatePlaces(parking.getNumberOfFreePrivatePlaces() - 1);
        } else {
            parking.setNumberOfFreeCommercialPlaces(parking.getNumberOfFreeCommercialPlaces() - 1);
        }
    }
}
