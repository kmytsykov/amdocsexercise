package com.mytsykov.amdocsexercise.service.impl;

import com.mytsykov.amdocsexercise.domain.Parking;
import com.mytsykov.amdocsexercise.dto.ParkingDto;
import com.mytsykov.amdocsexercise.dto.converter.ParkingDtoConverter;
import com.mytsykov.amdocsexercise.repository.ParkingRepository;
import com.mytsykov.amdocsexercise.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParkingServiceImpl implements ParkingService {

    private final ParkingRepository parkingRepository;

    private final ParkingDtoConverter parkingDtoConverter;

    @Autowired
    public ParkingServiceImpl(ParkingRepository parkingRepository,
                              ParkingDtoConverter parkingDtoConverter) {
        this.parkingRepository = parkingRepository;
        this.parkingDtoConverter = parkingDtoConverter;
    }

    @Override
    public ParkingDto create(ParkingDto parking) {
        Parking newParking = parkingDtoConverter.reverse().convert(parking);
        newParking.setNumberOfFreePrivatePlaces(newParking.getMaxNumberOfPrivateCars());
        newParking.setNumberOfFreeCommercialPlaces(newParking.getMaxNumberOfCommercialCars());
        newParking.setNumberOfFreeElectricPrivatePlaces(newParking.getMaxNumberOfElectricPrivateCars());
        newParking.setNumberOfFreeElectricCommercialPlaces(newParking.getMaxNumberOfElectricCommercialCars());
        newParking = parkingRepository.save(newParking);
        return parkingDtoConverter.convert(newParking);
    }
}
