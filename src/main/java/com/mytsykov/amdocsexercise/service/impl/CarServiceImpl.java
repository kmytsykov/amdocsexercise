package com.mytsykov.amdocsexercise.service.impl;

import com.mytsykov.amdocsexercise.domain.Car;
import com.mytsykov.amdocsexercise.dto.CarDto;
import com.mytsykov.amdocsexercise.dto.converter.CarDtoConverter;
import com.mytsykov.amdocsexercise.repository.CarRepository;
import com.mytsykov.amdocsexercise.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    private final CarDtoConverter carDtoConverter;

    @Autowired
    public CarServiceImpl(CarRepository carRepository,
                          CarDtoConverter carDtoConverter) {
        this.carRepository = carRepository;
        this.carDtoConverter = carDtoConverter;
    }

    @Override
    public CarDto create(CarDto car) {
        Car newCar = carDtoConverter.reverse().convert(car);
        newCar = carRepository.save(newCar);
        return carDtoConverter.convert(newCar);
    }
}
