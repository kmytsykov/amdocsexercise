package com.mytsykov.amdocsexercise.service;

import com.mytsykov.amdocsexercise.dto.CarDto;

import java.util.Optional;

public interface CarService {
    CarDto create(CarDto car);
}
