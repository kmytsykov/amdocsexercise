package com.mytsykov.amdocsexercise.service;

import com.mytsykov.amdocsexercise.dto.EnrollDto;
import com.mytsykov.amdocsexercise.dto.ParkingDto;

public interface EnrollService {
    ParkingDto enroll(Long carId, EnrollDto enrollData);
}
