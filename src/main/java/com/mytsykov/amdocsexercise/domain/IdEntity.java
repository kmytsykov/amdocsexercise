package com.mytsykov.amdocsexercise.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public class IdEntity {

    @Id
    @Column(name = "id")
    @GenericGenerator(
            name = "seq_id",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "prefer_sequence_per_entity", value = "true"),
                    @Parameter(name = "optimizer", value = "hilo")
            }
    )
    @GeneratedValue(generator = "seq_id")
    protected Long id;
}
