package com.mytsykov.amdocsexercise.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "parking")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Parking extends IdEntity {
    @Column
    private Double lat;
    @Column
    private Double lon;
    @Column
    private String name;
    @Column
    private Integer maxNumberOfPrivateCars;
    @Column
    private Integer maxNumberOfCommercialCars;
    @Column
    private Integer maxNumberOfElectricPrivateCars;
    @Column
    private Integer maxNumberOfElectricCommercialCars;
    @Column
    private Boolean dieselAllowed = Boolean.TRUE;

    @Column
    private Integer numberOfFreePrivatePlaces;
    @Column
    private Integer numberOfFreeCommercialPlaces;
    @Column
    private Integer numberOfFreeElectricPrivatePlaces;
    @Column
    private Integer numberOfFreeElectricCommercialPlaces;
}
