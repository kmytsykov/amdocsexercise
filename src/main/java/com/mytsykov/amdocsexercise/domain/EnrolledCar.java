package com.mytsykov.amdocsexercise.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "enrolled_cars")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnrolledCar extends IdEntity {

    @OneToOne(fetch = FetchType.LAZY)
    private Car car;

    @ManyToOne(fetch = FetchType.LAZY)
    private Parking parking;
}
