package com.mytsykov.amdocsexercise.domain.enums;

public enum CarType {
    PRIVATE,
    COMMERCIAL
}
