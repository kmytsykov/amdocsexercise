package com.mytsykov.amdocsexercise.domain.enums;

public enum EngineType {
    ELECTRIC,
    PETROL,
    DIESEL
}
